const express = require("express");
const Tarea = require("../models/tarea");

// Crea el router de tareas
const router = express.Router();

// GET /tareas
router.get("/", async (req, res) => {
  try {
    // Obtiene todas las tareas
    const tareas = await Tarea.find();
    // Retorna las tareas
    res.json(tareas);
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

// GET /tareas/id
router.get("/:id", async (req, res) => {
  // Obtiene el id
  let id = req.params.id;
  try {
    // Obtiene la trea por el id
    const tarea = await Tarea.findById(id);
    // Retorna la respuesta al cliente
    res.json(tarea);
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

// POST /tareas
router.post("/", async (req, res) => {
  try {
    // Crea el objeto
    const nuevaTarea = new Tarea({
      titulo: req.body.titulo,
      descripcion: req.body.descripcion,
      fecha: req.body.fecha,
    });
    // Guarda el objeto en BD
    let resultado = await nuevaTarea.save();
    // Retorna el objeto guardado al cliente
    res.json(resultado);
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

// DELETE /tareas/id
router.delete("/:id", async (req, res) => {
  // Obtiene el id
  let id = req.params.id;
  try {
    // Elimina la tarea por el id
    const resultado = await Tarea.remove({_id: id})
    // Retorna la respuesta al cliente
    res.json(resultado);
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});



module.exports = router;
