const express = require("express");

const Label = require("../models/label");


// Crea el router de tareas
const router = express.Router();

//router.get("/", async (req, res) => {
//    res.send("Hola labels");
//    }
//  );
router.get("/", async (req, res) => {
  try {
    // Obtiene todas las tareas
    const Labels = await Label.find();
    // Retorna las tareas
    res.json(Labels);
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

// GET /tareas/id
router.get("/:id", async (req, res) => {
    // Obtiene el id
    let id = req.params.id;
    try {
      // Obtiene la trea por el id
      const label = await Label.findById(id);
      // Retorna la respuesta al cliente
      res.json(label);
    } catch (error) {
      // retorna el error al cliente
      res.status(500).send(error);
    }
  });

// POST /tareas
router.post("/", async (req, res) => {
    try {
      // Crea el objeto
      const nuevoLabel = new Label({
        Label: req.body.Label
      });
      // Guarda el objeto en BD
      let resultado = await nuevoLabel.save();
      // Retorna el objeto guardado al cliente
      res.json(resultado);
    } catch (error) {
      // retorna el error al cliente
      res.status(500).send(error);
    }
  });

// DELETE /tareas/id
router.delete("/:id", async (req, res) => {
  // Obtiene el id
  let id = req.params.id;
  try {
    // Elimina la tarea por el id
    const resultado = await Label.remove({_id: id})
    // Retorna la respuesta al cliente
    res.json(resultado);
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

// PATCH /tareas/id
router.patch("/:id", async (req, res) => {
    // Obtiene el id de la tarea
    let id = req.params.id;
    try {
      // Actualiza la tarea en la BD
      const resultado = await Label.updateOne(
        { _id: id },
        {
          $set: req.body,
        }
      );
      // Retorna la respuesta al cliente
      return res.json(resultado);
    } catch (error) {
      return res.status(500).send(error);
    }
  });
  



module.exports = router;
