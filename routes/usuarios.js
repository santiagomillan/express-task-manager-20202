const express = require("express");
const tarea = require("../models/tarea");
const Usuario = require("../models/usuario");

// Crea el router de tareas
const router = express.Router();

router.post("/", async (req, res) => {
  try {
    // Crea el objeto
    const nuevoUsuario = new Usuario({
      user: req.body.user,
      password: req.body.password,
      nombre: req.body.nombre,
      email: req.body.email,
      fecha: req.body.fecha,
    });
    // Guarda el objeto en BD
    let resultado = await nuevoUsuario.save();
    // Retorna el objeto guardado al cliente
    res.json(resultado);
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

module.exports = router;
