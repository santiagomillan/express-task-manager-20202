const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors  = require("cors")

require("dotenv/config");

// Importa las rutas de la aplicación
const tareasRouter = require("./routes/tareas");
const usuariosRouter = require("./routes/usuarios");
const labelsRouter = require("./routes/labels");
const bookmarksRouter = require("./routes/bookmarks");

// crea la aplicación
const app = express();

// MIDDLEWARE
app.use(bodyParser.json());

// Middleware de rutas
app.use("/tareas", tareasRouter);
app.use("/usuarios", usuariosRouter);
app.use("/labels" , labelsRouter);
app.use("/bookmarks" , bookmarksRouter);

// Endpoint inicial
app.get("/", (req, res) => {
  res.send("Hola Express");
});

mongoose.connect(
  process.env.CONEXION_DB,
  { useUnifiedTopology: true, useNewUrlParser: true },
  () => {
    console.log("Conectado a la base de datos...");
  }
);

// Inicia la aplicación
app.listen(3000);
