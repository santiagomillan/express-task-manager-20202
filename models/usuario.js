const mongoose = require("mongoose");

const UsuarioSchema = mongoose.Schema({
  user: {
    type: String,
    required: true,
  },
  password: {
    type: String,
  },
  nombre: {
    type: String,
    required: true
  },
    email:{
      type: String,
      required: true
  },
  fecha: {
    type: Date,
    default: Date.now,
  }
});

// Exportar el esquema
module.exports = mongoose.model("Usuario", UsuarioSchema);

