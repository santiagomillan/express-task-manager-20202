const mongoose = require("mongoose");

const BookmarkSchema = mongoose.Schema({
  nombre: {
    type: String,
    required: true
  },
  url: {
      type: String,
      required: true
  },
  comentario: String,
  labels: {
      type: String,
      required: true
  },
  login: String,
  password: String,
  });
// Exportar el esquema
module.exports = mongoose.model("Bookmark", BookmarkSchema);
